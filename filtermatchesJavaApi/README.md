## run start api
```bash
 ./gradlew clean && ./gradlew build && docker-compose up
```
##hit the HealthCheck endpoint
```bash
    curl localhost:8081/actuator/health
```
## recreate image with jar
### stop containers
```bash
 docker stop filtermatchesjavaapi_app-server_1 filtermatchesjavaapi_db_1
```

### remove containers
```bash
 docker rm filtermatchesjavaapi_app-server_1 filtermatchesjavaapi_db_1
```

### remove image
```bash
 docker rmi filtermatchesjavaapi_app-server
```

### start application
```bash
 ./gradlew clean && ./gradlew build && docker-compose up
```

## run tests
```bash
 ./gradlew tests
```