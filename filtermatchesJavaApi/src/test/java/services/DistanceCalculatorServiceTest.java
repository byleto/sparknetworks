package services;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DistanceCalculatorServiceTest {
    @Test
    public void shouldReturnDistanceGivenLatitudeAndLongitudeForEastbourne() {
        DistanceCalculatorService distanceCalculatorService = new DistanceCalculatorService();
        double latitude = 50.768036; //Eastbourne
        double longitude =  0.290472;
        double expectedDistanceInKm =  254.76280407878355;

        double distance = distanceCalculatorService.getDistance(latitude, longitude);

        assertThat("distances are not equals", distance, is(expectedDistanceInKm));
    }

    @Test
    public void shouldReturnDistanceGivenLatitudeAndLongitudeForLondon() {
        DistanceCalculatorService distanceCalculatorService = new DistanceCalculatorService();
        double latitude =  51.509865; //London
        double longitude =  -0.118092;
        double expectedDistanceInKm =  211.90129165313158;

        double distance = distanceCalculatorService.getDistance(latitude, longitude);

        assertThat("distances are not equals", distance, is(expectedDistanceInKm));
    }

    @Test
    public void shouldReturnZeroGivenLatitudeAndLongitudeForCardiff() {
        DistanceCalculatorService distanceCalculatorService = new DistanceCalculatorService();
        double latitude =  51.481583; //Cardiff
        double longitude =  -3.179090;
        double expectedDistanceInKm =  0;

        double distance = distanceCalculatorService.getDistance(latitude, longitude);

        assertThat("distances are not equals", distance, is(expectedDistanceInKm));
    }
}