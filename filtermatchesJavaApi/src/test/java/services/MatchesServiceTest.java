package services;

import dtos.MatchFilter;
import dtos.MatchFilterBuilder;
import models.Match;
import models.MatchBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import repositories.MatchRepository;
import repositories.specifications.MatchSpecification;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest(MatchesService.class)
public class MatchesServiceTest {

    @Mock
    private MatchRepository matchRepository;

    @Mock
    private DistanceCalculatorService distanceCalculatorService;

    @Mock
    private MatchSpecification matchSpecification;

    @InjectMocks
    MatchesService matchesService;

    @Before
    public void setUp() throws Exception {
        matchSpecification = mock(MatchSpecification.class);
        whenNew(MatchSpecification.class).withAnyArguments().thenReturn(matchSpecification);
    }

    @Test
    public void shouldCallFindAllMethodFromRepositoryWithMatchSpecification() throws Exception {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .minAge(18)
                .maxAge(30)
                .build();

        Match match = new MatchBuilder().age(22).build();
        List<Match> matchesBetweenAgeRange = singletonList(match);
        when(matchRepository.findAll(matchSpecification)).thenReturn(matchesBetweenAgeRange);

        matchesService.getMatches(matchFilter);

        verify(matchRepository).findAll(matchSpecification);
    }

    @Test
    public void shouldFilterByDistanceWhenDistanceFilterExists() throws Exception {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .maximumDistance(30)
                .build();
        Match match = new MatchBuilder()
                .cityLatitude(50)
                .cityLongitude(60)
                .build();
        Match matchOutOfDistance = new MatchBuilder()
                .cityLatitude(100)
                .cityLongitude(200)
                .build();
        List<Match> matchesInDatabase = Arrays.asList(match, matchOutOfDistance);
        when(matchRepository.findAll(matchSpecification)).thenReturn(matchesInDatabase);
        when(distanceCalculatorService.getDistance(50, 60)).thenReturn(30d);
        when(distanceCalculatorService.getDistance(100, 200)).thenReturn(600d);

        List<Match> matches = matchesService.getMatches(matchFilter);

        verify(distanceCalculatorService).getDistance(50, 60);
        verify(distanceCalculatorService).getDistance(100, 200);
        assertThat(matches).containsExactly(match);
    }

    @Test
    public void shouldNotCallDistanceCalculatorServiceWhenMaximumDistanceIsNotPresent() throws Exception {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .build();
        Match match = new MatchBuilder()
                .cityLatitude(50)
                .cityLongitude(60)
                .build();

        List<Match> matchesInDatabase = singletonList(match);
        when(matchRepository.findAll(matchSpecification)).thenReturn(matchesInDatabase);

        matchesService.getMatches(matchFilter);

        verifyZeroInteractions(distanceCalculatorService);
    }
}
