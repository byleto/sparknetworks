package controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
public class MatchesControllerTest {

    private static final String SEARCH_ENPOINT = "/matches/search";

    @Autowired
    private MockMvc mvc;

    @Test
    public void getAllMatches() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/matches/search").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getMatchesByAgeRange() throws Exception {
        String urlTemplate = SEARCH_ENPOINT + "?minAge=18&maxAge=30";
        mvc.perform(MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getMatchesByHeightRange() throws Exception {
        String urlTemplate = SEARCH_ENPOINT + "?minHeight=170&maxHeight=180";
        mvc.perform(MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getMatchesByCompatibilityScoreRange() throws Exception {
        String urlTemplate = SEARCH_ENPOINT + "?minCompatibilityScore=0.60&maxCompatibilityScore=1";
        mvc.perform(MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getMatchesThatHasPhoto() throws Exception {
        String urlTemplate = SEARCH_ENPOINT + "?hasPhoto=true";
        mvc.perform(MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getMatchesWithMaximumDistance() throws Exception {
        String urlTemplate = SEARCH_ENPOINT + "?maximumDistance=30";
        mvc.perform(MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getFavouriteMatches() throws Exception {
        String urlTemplate = SEARCH_ENPOINT + "?favourite=true";
        mvc.perform(MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getMatchesWithoutContacts() throws Exception {
        String urlTemplate = SEARCH_ENPOINT + "?inContact=false";
        mvc.perform(MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getMatchesWithAllParameters() throws Exception {
        String parameters = "?minAge=18" +
                "&maxAge=30" +
                "&minHeight=170" +
                "&maxHeight=180" +
                "&minCompatibilityScore=0.65" +
                "&maxCompatibilityScore=1" +
                "&maximumDistance=30" +
                "&favourite=true" +
                "&inContact=false";
        String urlTemplate = SEARCH_ENPOINT + parameters;
        mvc.perform(MockMvcRequestBuilders.get(urlTemplate).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}