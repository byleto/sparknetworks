package repositories.specifications;

import dtos.MatchFilter;
import dtos.MatchFilterBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.data.jpa.domain.Specification;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MatchSpecificationTest {

    private MatchSpecification matchSpecification;

    @Test
    void shouldBeAnInstanceOfSpecification() {
        MatchFilter matchFilter = new MatchFilterBuilder().build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        assertThat(matchSpecification, is(instanceOf(Specification.class)));
    }
}

