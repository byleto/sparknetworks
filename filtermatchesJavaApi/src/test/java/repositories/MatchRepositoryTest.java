package repositories;

import controllers.Application;
import dtos.MatchFilter;
import dtos.MatchFilterBuilder;
import models.Match;
import models.MatchBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import repositories.specifications.MatchSpecification;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = Application.class)
public class MatchRepositoryTest {
    @Autowired
    private MatchRepository matchRepository;
    private Match match;
    private Match secondMatch;
    private Match thirdMatch;
    private List<Match> allMatches;

    @Before
    public void setUp() {
        match = new MatchBuilder()
                .age(18)
                .heightInCm(155)
                .compatibilityScore((float) 0.50)
                .photo("")
                .favourite(true)
                .contactsExchanged(0)
                .build();
        secondMatch = new MatchBuilder()
                .age(22)
                .heightInCm(160)
                .compatibilityScore((float) 0.75)
                .photo(null)
                .favourite(false)
                .contactsExchanged(10)
                .build();
        thirdMatch = new MatchBuilder()
                .age(35)
                .heightInCm(172)
                .compatibilityScore((float) 0.80)
                .photo("photo-url")
                .contactsExchanged(12)
                .build();
        allMatches = Arrays.asList(match, secondMatch, thirdMatch);
        matchRepository.saveAll(allMatches);
    }

    @Test
    public void shouldFindMatchInAgeRange() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .minAge(18)
                .maxAge(30)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).containsExactly(match, secondMatch);
    }

    @Test
    public void shouldFindMatchInHeightRange() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .minHeight(170)
                .maxHeight(180)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).containsExactly(thirdMatch);
    }

    @Test
    public void shouldFindMatchInCompatibilityScoreRange() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .minCompatibilityScore((float) 0.75)
                .maxCompatibilityScore((float) 1)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).containsExactly(secondMatch, thirdMatch);
    }

    @Test
    public void shouldFindMatchesWithPhoto() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .hasPhoto(true)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).containsExactly(thirdMatch);
    }

    @Test
    public void shouldNotFilterMatchesWithoutPhotoWhenHasPhotoFilterIsFalse() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .hasPhoto(false)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).isEqualTo(allMatches);
    }

    @Test
    public void shouldNotFilterMatchesWithoutPhotoWhenHasPhotoFilterIsNull() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).isEqualTo(allMatches);
    }

    @Test
    public void shouldFindFavouriteMatches() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .favourite(true)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).containsExactly(match);
    }

    @Test
    public void shouldNotFilterNonFavouritesMatchesWhenFavouriteFilterIsFalse() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .favourite(false)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).isEqualTo(allMatches);
    }

    @Test
    public void shouldNotFilterNonFavouritesMatchesWhenFavouriteFilterIsNull() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).isEqualTo(allMatches);
    }

    @Test
    public void shouldFindMatchesWithContacts() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .inContact(true)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).containsExactly(secondMatch, thirdMatch);
    }

    @Test
    public void shouldNotFilterMatchesWithoutContactsWhenInContactFilterIsFalse() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .inContact(false)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).isEqualTo(allMatches);
    }

    @Test
    public void shouldNotFilterMatchesWithoutContactsWhenInContactFilterIsNull() {
        MatchFilter matchFilter = new MatchFilterBuilder()
                .inContact(false)
                .build();
        MatchSpecification matchSpecification = new MatchSpecification(matchFilter);

        List<Match> filterMatches = matchRepository.findAll(matchSpecification);

        assertThat(filterMatches).isEqualTo(allMatches);
    }
}