CREATE TABLE IF NOT EXISTS matches(
    id int(5) NOT NULL AUTO_INCREMENT,
    display_name varchar(100) DEFAULT NULL,
    age INT,
    job_title varchar(159) DEFAULT NULL,
    height_in_Cm INT,
    city_name varchar(150) DEFAULT NULL,
    city_latitude FLOAT,
    city_longitude FLOAT,
    photo varchar(250) DEFAULT NULL,
    compatibility_score DOUBLE,
    contacts_exchanged INT,
    favourite bool DEFAULT NULL,
    religion varchar(50) DEFAULT NULL,
    PRIMARY KEY(id)
 );