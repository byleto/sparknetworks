package services;

import dtos.MatchFilter;
import models.Match;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import repositories.MatchRepository;
import repositories.specifications.MatchSpecification;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MatchesService {
    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    DistanceCalculatorService distanceCalculatorService;

    public List<Match> getMatches(MatchFilter matchFilter) {
        Specification<Match> matchSpecification = new MatchSpecification(matchFilter);
        List<Match> matches = matchRepository.findAll(matchSpecification);
        if (null != matchFilter.getMaximumDistance()) {
            return matches
                    .parallelStream()
                    .filter(match -> distanceCalculatorService.getDistance(match.getCityLatitude(), match.getCityLongitude()) <= matchFilter.getMaximumDistance())
                    .collect(Collectors.toList());
        }
        return matches;
    }
}
