package services;

import org.springframework.stereotype.Service;

@Service
public class DistanceCalculatorService {
    //latitude and longitude for Cardiff City
    private final double MY_LATITUDE = 51.481583;
    private final double MY_LONGITUDE = -3.179090;

    public double getDistance(double latitude, double longitude) {
        if (latitude == MY_LATITUDE && longitude == MY_LONGITUDE) {
            return 0;
        }
        double theta = MY_LONGITUDE - longitude;
        double distance = Math.sin(Math.toRadians(MY_LATITUDE))
                * Math.sin(Math.toRadians(latitude))
                + Math.cos(Math.toRadians(MY_LATITUDE))
                * Math.cos(Math.toRadians(latitude))
                * Math.cos(Math.toRadians(theta));
        distance = Math.acos(distance);
        distance = Math.toDegrees(distance);
        distance = distance * 60 * 1.1515;

        return distance * 1.609344;
    }

    public DistanceCalculatorService() {
    }
}