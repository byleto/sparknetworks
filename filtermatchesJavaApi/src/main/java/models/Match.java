package models;

import javax.persistence.*;

@Entity
@Table(name = "matches")
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String displayName;
    private int age;
    private String jobTitle;
    private int heightInCm;
    private String cityName;
    private float cityLatitude;
    private float cityLongitude;
    private String photo;
    private double compatibilityScore;
    private int contactsExchanged;
    private boolean favourite;
    private String religion;

    public int getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getAge() {
        return age;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public int getHeightInCm() {
        return heightInCm;
    }

    public String getCityName() {
        return cityName;
    }

    public float getCityLatitude() {
        return cityLatitude;
    }

    public float getCityLongitude() {
        return cityLongitude;
    }

    public String getPhoto() {
        return photo;
    }

    public double getCompatibilityScore() {
        return compatibilityScore;
    }

    public int getContactsExchanged() {
        return contactsExchanged;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public String getReligion() {
        return religion;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setHeightInCm(int heightInCm) {
        this.heightInCm = heightInCm;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setCityLatitude(float cityLatitude) {
        this.cityLatitude = cityLatitude;
    }

    public void setCityLongitude(float cityLongitude) {
        this.cityLongitude = cityLongitude;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setCompatibilityScore(double compatibilityScore) {
        this.compatibilityScore = compatibilityScore;
    }

    public void setContactsExchanged(int contactsExchanged) {
        this.contactsExchanged = contactsExchanged;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public Match() {
    }
}