package models;

public class MatchBuilder {
    private final Match match;

    public MatchBuilder() {
        this.match = new Match();
    }

    public static MatchBuilder match() {
        return new MatchBuilder();
    }

    public MatchBuilder age(int age) {
        match.setAge(age);
        return this;
    }

    public MatchBuilder photo(String photo) {
        match.setPhoto(photo);
        return this;
    }

    public MatchBuilder displayName(String displayName) {
        match.setDisplayName(displayName);
        return this;
    }

    public MatchBuilder jobTitle(String jobTitle) {
        match.setJobTitle(jobTitle);
        return this;
    }

    public MatchBuilder cityName(String cityName) {
        match.setCityName(cityName);
        return this;
    }

    public MatchBuilder religion(String religion) {
        match.setReligion(religion);
        return this;
    }

    public MatchBuilder cityLatitude(float cityLatitude) {
        match.setCityLatitude(cityLatitude);
        return this;
    }

    public MatchBuilder cityLongitude(float cityLongitude) {
        match.setCityLongitude(cityLongitude);
        return this;
    }

    public MatchBuilder compatibilityScore(float compatibilityScore) {
        match.setCompatibilityScore(compatibilityScore);
        return this;
    }

    public MatchBuilder heightInCm(int heightInCm) {
        match.setHeightInCm(heightInCm);
        return this;
    }

    public MatchBuilder contactsExchanged(int contactsExchanged) {
        match.setContactsExchanged(contactsExchanged);
        return this;
    }

    public MatchBuilder favourite(boolean favourite) {
        match.setFavourite(favourite);
        return this;
    }

    public Match build()  {
        return match;
    }
}
