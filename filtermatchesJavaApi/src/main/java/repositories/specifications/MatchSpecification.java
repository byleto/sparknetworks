package repositories.specifications;

import dtos.MatchFilter;
import models.Match;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class MatchSpecification implements Specification<Match> {

    private MatchFilter matchFilter;

    public MatchSpecification(MatchFilter matchFilter) {
        super();
        this.matchFilter = matchFilter;
    }

    private void addBetweenPredicate(Predicate predicate, CriteriaBuilder criteriaBuilder, Root<Match> root, String field, Integer minValue, Integer maxValue) {
        if (null != minValue && null != maxValue) {
            predicate.getExpressions()
                    .add(criteriaBuilder
                            .between(root.get(field),
                                    minValue,
                                    maxValue));
        }
    }

    @Override
    public Predicate toPredicate(Root<Match> root, CriteriaQuery<?> cq,
                                 CriteriaBuilder criteriaBuilder) {
        Predicate predicate = criteriaBuilder.conjunction();
        Integer minAge = this.matchFilter.getMinAge();
        Integer maxAge = this.matchFilter.getMaxAge();
        addBetweenPredicate(predicate, criteriaBuilder, root, "age", minAge, maxAge);

        Integer minHeight = this.matchFilter.getMinHeight();
        Integer maxHeight = this.matchFilter.getMaxHeight();
        addBetweenPredicate(predicate, criteriaBuilder, root, "heightInCm", minHeight, maxHeight);

        Float minCompatibilityScore = this.matchFilter.getMinCompatibilityScore();
        Float maxCompatibilityScore = this.matchFilter.getMaxCompatibilityScore();
        if (null != minCompatibilityScore && null != maxCompatibilityScore) {
            predicate.getExpressions()
                    .add(criteriaBuilder
                            .between(root.get("compatibilityScore"),
                                    minCompatibilityScore,
                                    maxCompatibilityScore));
        }
        Boolean hasPhoto = this.matchFilter.getHasPhoto();
        if (null != hasPhoto && hasPhoto) {
            predicate.getExpressions()
                    .add(criteriaBuilder.and(
                            criteriaBuilder.isNotNull(root.get("photo"))
                            , criteriaBuilder.notEqual(root.get("photo"), "")));
        }
        Boolean favourite = this.matchFilter.isFavourite();
        if (null != favourite && favourite) {
            predicate.getExpressions()
                    .add(criteriaBuilder.equal(root.get("favourite"), true));
        }
        Boolean inContact = this.matchFilter.isInContact();
        if (null != inContact && inContact) {
            predicate.getExpressions()
                    .add(criteriaBuilder.greaterThan(root.get("contactsExchanged"), 0));
        }
        return predicate;
    }
}