package dtos;

public class MatchFilterBuilder {
    private final MatchFilter matchFilter;

    public MatchFilterBuilder() {
        this.matchFilter = new MatchFilter();
    }

    public static MatchFilterBuilder matchFilter() {
        return new MatchFilterBuilder();
    }

    public MatchFilterBuilder minAge(Integer minAge) {
        matchFilter.setMinAge(minAge);
        return this;
    }

    public MatchFilterBuilder maxAge(Integer maxAge) {
        matchFilter.setMaxAge(maxAge);
        return this;
    }

    public MatchFilterBuilder minHeight(Integer minHeight) {
        matchFilter.setMinHeight(minHeight);
        return this;
    }

    public MatchFilterBuilder maxHeight(Integer maxHeight) {
        matchFilter.setMaxHeight(maxHeight);
        return this;
    }

    public MatchFilterBuilder minCompatibilityScore(Float minCompatibilityScore) {
        matchFilter.setMinCompatibilityScore(minCompatibilityScore);
        return this;
    }

    public MatchFilterBuilder maxCompatibilityScore(Float maxCompatibilityScore) {
        matchFilter.setMaxCompatibilityScore(maxCompatibilityScore);
        return this;
    }

    public MatchFilterBuilder hasPhoto(Boolean hasPhoto) {
        matchFilter.setHasPhoto(hasPhoto);
        return this;
    }

    public MatchFilterBuilder maximumDistance(Integer maximumDistance) {
        matchFilter.setMaximumDistance(maximumDistance);
        return this;
    }

    public MatchFilterBuilder favourite(Boolean favourite) {
        matchFilter.setFavourite(favourite);
        return this;
    }

    public MatchFilterBuilder inContact(Boolean inContact) {
        matchFilter.setInContact(inContact);
        return this;
    }

    public MatchFilter build() {
        return matchFilter;
    }
}
