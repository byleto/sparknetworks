package dtos;

public class MatchFilter {
    private Integer maxAge;
    private Integer minAge;
    private Integer minHeight;
    private Integer maxHeight;
    private Float minCompatibilityScore;
    private Float maxCompatibilityScore;
    private Boolean hasPhoto;
    private Integer maximumDistance;
    private Boolean favourite;
    private Boolean inContact;

    public MatchFilter() {
    }

    public MatchFilter(Integer minAge, Integer maxAge, Integer minHeight, Integer maxHeight,
                       Float minCompatibilityScore, Float maxCompatibilityScore, Boolean hasPhoto,
                       Integer maximumDistance, Boolean favourite, Boolean inContact) {
        this.maxAge = maxAge;
        this.minAge = minAge;
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        this.minCompatibilityScore = minCompatibilityScore;
        this.maxCompatibilityScore = maxCompatibilityScore;
        this.hasPhoto = hasPhoto;
        this.maximumDistance = maximumDistance;
        this.favourite = favourite;
        this.inContact = inContact;
    }

    public Boolean isInContact() {
        return inContact;
    }

    public Boolean isFavourite() {
        return favourite;
    }

    public Integer getMaximumDistance() {
        return maximumDistance;
    }

    public Boolean getHasPhoto() {
        return hasPhoto;
    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public Integer getMinHeight() {
        return minHeight;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public void setMinHeight(int minHeight) {
        this.minHeight = minHeight;
    }

    public void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    public Float getMinCompatibilityScore() {
        return minCompatibilityScore;
    }

    public Float getMaxCompatibilityScore() {
        return maxCompatibilityScore;
    }

    public void setMinCompatibilityScore(Float minCompatibilityScore) {
        this.minCompatibilityScore = minCompatibilityScore;
    }

    public void setMaxCompatibilityScore(Float maxCompatibilityScore) {
        this.maxCompatibilityScore = maxCompatibilityScore;
    }

    public void setHasPhoto(Boolean hasPhoto) {
        this.hasPhoto = hasPhoto;
    }

    public void setMaximumDistance(Integer maximumDistance) {
        this.maximumDistance = maximumDistance;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

    public void setInContact(Boolean inContact) {
        this.inContact = inContact;
    }
}
