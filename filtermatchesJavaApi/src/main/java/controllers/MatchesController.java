package controllers;

import dtos.MatchFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import services.MatchesService;

import java.util.HashMap;

@Controller
@RequestMapping(path = "/matches")
public class MatchesController {
    @Autowired
    private MatchesService matchesService;

    @GetMapping(path = "/search")
    public @ResponseBody
    HashMap<String, Object> getMatches(@RequestParam(required = false, value = "minAge") final Integer minAge,
                                       @RequestParam(required = false, value = "maxAge") final Integer maxAge,
                                       @RequestParam(required = false, value = "minHeight") final Integer minHeight,
                                       @RequestParam(required = false, value = "maxHeight") final Integer maxHeight,
                                       @RequestParam(required = false, value = "minCompatibilityScore") final Float minCompatibilityScore,
                                       @RequestParam(required = false, value = "maxCompatibilityScore") final Float maxCompatibilityScore,
                                       @RequestParam(required = false, value = "hasPhoto") final Boolean hasPhoto,
                                       @RequestParam(required = false, value = "favourite") final Boolean favourite,
                                       @RequestParam(required = false, value = "inContact") final Boolean inContact,
                                       @RequestParam(required = false, value = "maximumDistance") final Integer maximumDistance) {
        HashMap<String, Object> responseBody = new HashMap<>();
        MatchFilter matchFilter = new MatchFilter(minAge, maxAge,
                minHeight, maxHeight, minCompatibilityScore,
                maxCompatibilityScore, hasPhoto, maximumDistance, favourite, inContact);
        responseBody.put("matches", matchesService.getMatches(matchFilter));
        return responseBody;
    }
}