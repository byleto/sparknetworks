## Filtering Matches Exercise
This application allows you to filter on a prebuilt set of filters (using server side filtering)

### Filtering Details

| Filter | Details |
|--------|---------|
| Has photo | Boolean yes / no filter |
| In contact | Boolean yes / no filter |
| Favourite | Boolean yes / no filter |
| Compatibility Score | range: from 1% to 99% |
| age | range: from 18 years old until > 95 years old |
| height | range: from 135cm to > 210cm |
| distance in km | single selection: lower bound < 30 km, upper bound > 300 km | 

For the distance filter it is in use Cardiff as current location.
* Latitude = 51.481583;
* Longitude = -3.179090;

### requirements
* Docker
* Java 8

## Server side
The API runs in docker using two containers, one for the the database and the other one to run the API  
The containers were defined using docker-compose

### run the API 
```bash
 ./gradlew clean && ./gradlew build && docker-compose up
```
### hit the HealthCheck endpoint
```bash
    curl localhost:8081/actuator/health
```

When you run the for the very first time, could take a while because Docker is downloading the images to create the containers. 
**Containers:**
* filtermatchesjavaapi_app-server_1 -> Container based on and image with Java8 defined by Dockerfile to execute the JAR application, depends on db container to starts
* filtermatchesjavaapi_db_1 -> Defined in the docker-compose file , based on a mysql5-7 image, the creation of tables and data population is being managed by spring
The API runs in the port 8081

## Tests
**The test strategy for the API is the following**
* Controllers -> Integration Tests using SpringBootTest
* Services -> Unit Tests (JUnit5, Mockito, PowerMock
* Repositories and Specification -> Integration tests using DataJpaTest

### Client side
### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />

There is a config called file in the UI application to managel the maximum and minimum values of all filters

## Storybook`
Runs storybook, with storybook you can see the components in and isoleted way.
Open [http://localhost:9009] (http://localhost:9009) to view it in the browser.

## Tests
All the component are being tested using snapshoots.

The apllication runs in the por 3000 you will need the API running to make request using filters and to see the matches when the UI starts. 

## TO DOS (tech debt)
* Improve scritps to start API
* Implement chain of responsibility pattern to manage filters in the API
* Add integration test between components in UI
* Create E2E test 
* Refactor components to use hooks
* Extact fech logic from components to a different file