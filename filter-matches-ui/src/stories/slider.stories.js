import React from 'react';
import { storiesOf } from '@storybook/react';
import 'rc-slider/assets/index.css';
import RangeFilter from '../components/RangeFilter'
import SliderFilter from '../components/SliderFilter'
import ToggleFilter from '../components/ToggleFilter'
storiesOf('Filter Components', module)
    .add('range filter', () => (
        <RangeFilter label={'Age'} min={18} max={55}/>
    ))
    .add('slider filter', () => (
        <SliderFilter label={'Distance'} min={30} max={55}/>
    ))
    .add('toggle filter', () => (
        <ToggleFilter label={'Has Photo'}/>
    ));
