import React from 'react';
import { storiesOf } from '@storybook/react';
import MatchViewer from "../components/MatchViewer";

const match = {
    "id":1,
    "displayName": "Kysha",
    "age": 45,
    "jobTitle": "Lawyer",
    "heightInCm": 144,
    "cityName": "London",
    "compatibilityScore": 0.88,
    "contactsExchanged": 10,
    "favourite": true,
    "religion": "Islam"
}
storiesOf('Match Viewer', module)
    .add('Match Viewer with props', () => (
        <MatchViewer match={match}>
        </MatchViewer>
    ));
