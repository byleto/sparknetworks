import React from 'react';
import { storiesOf } from '@storybook/react';
import SearchResult from "../components/SearchResult.js";
import matchesMock from "../components/__test__/matchesMock.js";

storiesOf('Search Result', module)
    .add('Search Result with matches', () => (
        <SearchResult matches = {matchesMock.matches} > </SearchResult>
    ));
