import React from 'react';
import './App.css';
import MatchesSearcher from './components/MatchesSearcher';

function App() {
  return (
    <div className="App">
      <MatchesSearcher/>
    </div>
  );
}

export default App;
