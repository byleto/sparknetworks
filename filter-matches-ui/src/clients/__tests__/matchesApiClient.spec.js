import fetchMatches from '../matchesApiClient'

describe('matchesApiClient', () => {
  it('returns an object if status code is ok', () => {
    window.fetch = jest.fn().mockImplementation(() => ({
      status: 200,
      json: () => new Promise((resolve, reject) => {
        resolve({
          matches: [],
        })
      }),
    }))

    expect(fetchMatches()).resolves.toEqual({ matches: [] })
  })
})