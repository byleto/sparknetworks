const fetchMatches = async (parameters) => {
  const endpoint = `/matches/search?${parameters}`
  return (fetch(endpoint)
    .then(res => res.json())
    .catch((e) => {
      throw(new Error('Error fetching matches'))
    })
    .then((data => data.matches)))
  }

  

  export default fetchMatches