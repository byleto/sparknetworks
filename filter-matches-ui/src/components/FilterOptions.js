import React, { Component } from 'react'
import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button'
import RangeFilter from './RangeFilter';
import { 
    MAX_AGE, 
    MIN_AGE, 
    MAX_HEIGHT, 
    MIN_HEIGHT,
    MIN_COMPATIBILITY_SCORE,
    MAX_COMPATIBILITY_SCORE,
    MIN_DISTANCE,
    MAX_DISTANCE
 } from '../configs/filterConfigs'
import ToggleFilter from './ToggleFilter';
import SliderFilter from './SliderFilter';
import querystring  from 'querystring';
import fetchMatches from '../clients/matchesApiClient'

class FilterOptions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ageRange: [MIN_AGE, MAX_AGE],
            heightRange: [MIN_HEIGHT, MAX_HEIGHT],
            compatibilityScoreRange: [MIN_COMPATIBILITY_SCORE, MAX_COMPATIBILITY_SCORE],
            hasPhoto: 0,
            maximumDistance: MAX_DISTANCE,
            favourite: 0,
            inContact: 0
        };
        this.handleInputChange = this.handleInputChange.bind(this);
     }
 
     async handleSubmit() {
        const { ageRange, heightRange, compatibilityScoreRange, hasPhoto, maximumDistance, favourite, inContact } = this.state
        const params = {
            minAge: ageRange[0],
            maxAge: ageRange[1],
            minHeight: heightRange[0],
            maxHeight: heightRange[1],
            minCompatibilityScore: compatibilityScoreRange[0]/100,
            maxCompatibilityScore: compatibilityScoreRange[1]/100,
            hasPhoto,
            maximumDistance,
            favourite,
            inContact
        }
        const queryParameters = querystring.stringify(params);
        try {
            const matches = await fetchMatches(queryParameters)
            this.props.updateMatches(matches);
            this.setState({ totalMatches: matches.length})
        } catch(err) {
            this.props.updateErrorStatus(err.message);
        }
    }

    handleInputChange(field, value) {
        this.setState({
            [field]: value
        });
    }

    renderRangeFilter(name, label, min, max) {
        return (
            <div className="col-sm-6 col-md-4">
                <RangeFilter 
                    onChange={this.handleInputChange.bind(this)} 
                    name={name} 
                    label={label} 
                    min={min} 
                    max={max}
                />             
            </div>
        )
    }

    renderToggleFilter(name, label) {
        return(
            <div className="col-sm-6 col-md-4">
                <ToggleFilter 
                    name={name} 
                    onChange={this.handleInputChange.bind(this)}
                    label={label}
                /> 
            </div>
        )
    }

    renderSliderFilter(name, label, min, max) {
        return(
            <div className="col-sm-6 col-md-4">
                <SliderFilter 
                    name={name} 
                    onChange={this.handleInputChange.bind(this)}
                    label={label}
                    min={min}
                    max={max}
                /> 
        </div>
        )
    }

    renderButton() {
        return (
                <div className={"col-1"}>
                    <Button
                        variant="primary"
                        onClick={this.handleSubmit.bind(this)}
                    > 
                        Search
                    </Button>
                </div>
        )
    }

    render() {
        return( 
            <div style={{border:'1px'}} className="container">
                <div className="row">
                    <h4>Filter Options</h4> 
                </div>
                <div className="row">
                        <strong>Profile</strong> 
                </div>
                <div className="row">
                    {this.renderRangeFilter('ageRange', 'Age', MIN_AGE, MAX_AGE)}
                    {this.renderRangeFilter('heightRange', 'Height in Cm', MIN_HEIGHT, MAX_HEIGHT)}
                    {this.renderRangeFilter('compatibilityScoreRange', 'Compatibility Score %', MIN_COMPATIBILITY_SCORE, MAX_COMPATIBILITY_SCORE)}
                </div>
                <div className="row">
                        <strong>Account</strong> 
                </div>
                <div className="row">
                    {this.renderToggleFilter('hasPhoto', 'Has photo')}
                    {this.renderToggleFilter('favourite', 'Favourite')}
                    {this.renderToggleFilter('inContact', 'In contact')}
                </div>
                <div className="row">
                        <strong>Discovery</strong> 
                </div>
                <div className="row">
                    {this.renderSliderFilter('maximumDistance', 'Maximun Distance', MIN_DISTANCE, MAX_DISTANCE)}
                </div>
                <div className="row">
                        <h4>Look for your matches</h4> 
                </div>
                <div className={"row align-items-start"}>
                    {this.renderButton()}
                </div>
            </div>
        )
    }
}

FilterOptions.propTypes = {
    totalMatches: PropTypes.number,
    updateErrorStatus: PropTypes.func,
    updateMatches: PropTypes.func
};

FilterOptions.defaultProps = {
    totalMatches: 0,
    updateMatches: () => {},
    updateErrorStatus: () => {}
};

export default FilterOptions