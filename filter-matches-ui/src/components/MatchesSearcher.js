import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchResult from "./SearchResult";
import FilterOptions from './FilterOptions';
import fetchMatches from '../clients/matchesApiClient'
class MatchesSearcher extends Component {
    constructor(props) {
        super(props);
        const { matches } = this.props;
        this.state = {
            matches,
            errorStatus: ''
        }
    }

    updateMatches(matches) {
        this.setState( { matches })
    }

    updateErrorStatus(errorStatus) {
        this.setState( { errorStatus })
    }

    async componentDidMount() {
        try {
            const matches = await fetchMatches()
            this.setState({ matches })
        } catch(err) {
            this.setState({errorStatus: err.message})
        }
    }

    render () {
        const { matches }  = this.state;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                            <FilterOptions 
                                totalMatches={matches ? matches.length : 0} 
                                updateMatches={this.updateMatches.bind(this)}
                                updateErrorStatus={this.updateErrorStatus.bind(this)}
                            />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <SearchResult
                            matches={matches}
                        />
                    </div>
                </div>
            </div>
        )
    };
}

export default MatchesSearcher
