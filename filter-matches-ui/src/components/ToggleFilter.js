
import React, { Component } from 'react'
import Switch from 'react-input-switch';
import PropTypes from 'prop-types'
import Card from 'react-bootstrap/Card';

class ToggleFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
        };
    }

    onToggleChange = (value) => {
        this.setState({
          value,
        });
        this.props.onChange(this.props.name, value);
    }

    render(){
        return (
            <Card style={{ width: '17rem' }}>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 align-bottom">
                           <strong>{this.props.label}</strong> 
                        </div>
                        <div className="col-sm-12 align-bottom">
                            <Switch value={this.state.value} onChange={this.onToggleChange} />
                        </div>
                    </div>
                </div>
            </Card>
        )
    }
  }

ToggleFilter.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    onChange: PropTypes.func
}

ToggleFilter.defaultProps = {
    label: '',
    name: '',
    onChange: () => {}
}

export default ToggleFilter