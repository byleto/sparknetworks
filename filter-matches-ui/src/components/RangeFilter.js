import React from 'react'
import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'rc-slider/assets/index.css';
import Slider from 'rc-slider'
import Card from 'react-bootstrap/Card';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

class RangeFilter extends React.Component {
    constructor(props) {
        super(props);
        const { min, max } = props
        this.state = {
            value: [min, max],
        };
    }

    onSliderChange = (value) => {
      this.setState({
        value,
      });
      this.props.onChange(this.props.name, value);
    }

    render() {
        const { min, max, label } = this.props
        return (
            <Card style={{ width: '17rem' }}>
                <div className="container align-middle">
                    <div className="row">
                        <div className="col-sm-12">
                            <label><strong>{label}</strong></label>
                        </div>
                        <div className="col-sm-10 offset-sm-1">
                            <Range 
                                allowCross={false} 
                                min={min} max={max} 
                                value={this.state.value} 
                                onChange={this.onSliderChange} 
                            />
                        </div>
                        <div className="col-sm-12">
                            <label>
                                {`Between: ${this.state.value[0]} and ${this.state.value[1]}`}
                            </label> 
                        </div>
                    </div>
                    </div>
            </Card>
        );
    }
  }

RangeFilter.propTypes = {
    label: PropTypes.string,
    min: PropTypes.number,
    max: PropTypes.number,
    name: PropTypes.string,
    onChange: PropTypes.func
}

RangeFilter.defaultProps = {
    label: '',
    min: 0,
    max: 0,
    name: '',
    onChange: () => {}
}

export default RangeFilter