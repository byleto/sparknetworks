import React from 'react'
import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.min.css';

const CreateRowWithTwoColumns = (description, value, suffix="") => {
    return <div className="row">
        <div className="col-7">
           <label>{description}</label> 
        </div>
        <div className="col-5 ">
            <label>{`${value}${suffix}`}</label>
        </div>
    </div>
};

const CreateRowWithOneColumn = description => {
    return <div className="row">
        <div className="row">
            <div className="col-12">
                <label><strong>{description}</strong></label>
            </div>
        </div>
    </div>
};

const MatchViewer = props => {
    const {
     match: {
        displayName,
        age,
        jobTitle,
        heightInCm,
        cityName,
        photo,
        compatibilityScore,
        contactsExchanged,
        favourite,
        religion
     }
    } = props;
    const compatibilityScorePercentage = compatibilityScore * 100;
    const imageNotFoundUrl = '../../image-not-found.png'
    return (
        <Card style={{ width: '17rem' }}>
            <Card.Img variant="top" height={136} width={270} src={photo ? photo : imageNotFoundUrl} />
            <Card.Body>
            <Card.Title>{displayName}, {age}</Card.Title>
                <div className="container">
                    {CreateRowWithOneColumn(jobTitle)}
                    {CreateRowWithOneColumn(cityName)}
                    {CreateRowWithTwoColumns("Height:", heightInCm, " Cm")}
                    {CreateRowWithTwoColumns("Compatibility:", compatibilityScorePercentage, "%")}
                    {CreateRowWithTwoColumns("Contacts:", contactsExchanged)}
                    {CreateRowWithTwoColumns("Favourite:", String(favourite))}
                    {CreateRowWithTwoColumns("Religion:", religion)}
                </div>
            </Card.Body>
        </Card>
    )
};

MatchViewer.propTypes = {
    match: PropTypes.object
};

MatchViewer.defaultProps = {
    match: {}
};

export default MatchViewer
