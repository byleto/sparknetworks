import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import SliderFilter from "../SliderFilter.js";

describe('Slider Filter test', () => {
    it('should render successfully', () => {
        expect(new ShallowRenderer().render(<SliderFilter label={'distance'} min={0} max={100}/>)).toMatchSnapshot()
    });
});
