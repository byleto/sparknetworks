import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import RangeFilter from "../RangeFilter.js";

describe('Range Filter test', () => {
    it('should render successfully', () => {
        expect(new ShallowRenderer().render(<RangeFilter label={'age'} min={0} max={100}/>)).toMatchSnapshot()
    });
});
