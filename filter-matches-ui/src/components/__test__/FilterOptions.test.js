import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import FilterOptions from "../FilterOptions.js";

describe('Filter Options test', () => {
    it('should render successfully', () => {
        expect(new ShallowRenderer().render(<FilterOptions />)).toMatchSnapshot()
    });

    it('default properties test', () => {
        FilterOptions.defaultProps.updateErrorStatus()
        FilterOptions.defaultProps.updateMatches()
    })
});
