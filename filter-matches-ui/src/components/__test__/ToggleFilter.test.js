import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import ToggleFilter from "../ToggleFilter.js";

describe('Toggle Filter test', () => {
    it('should render successfully', () => {
        expect(new ShallowRenderer().render(<ToggleFilter label={'Has photo'} />)).toMatchSnapshot()
    });
});
