import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import MatchesSearcher from "../MatchesSearcher";

describe('MatchesSearcher test', () => {
    it('should render successfully', () => {
        expect(new ShallowRenderer().render(<MatchesSearcher />)).toMatchSnapshot()
    });
});
