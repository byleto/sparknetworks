import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import MatchViewer from '../MatchViewer'

describe('MatchViewer test', () => {
    it('should render successfully', () => {
        const match = {
            "id":1,
            "displayName": "Kysha",
            "age": 45,
            "jobTitle": "Lawyer",
            "heightInCm": 144,
            "cityName": "London",
            "compatibilityScore": 0.88,
            "contactsExchanged": 10,
            "favourite": true,
            "religion": "Islam"
        }
        expect(new ShallowRenderer().render(<MatchViewer match={match}/>)).toMatchSnapshot()
    });
});
