import React from 'react'
import ShallowRenderer from 'react-test-renderer/shallow'
import SearchResult from "../SearchResult.js";

describe('Search Result test', () => {
    it('should render successfully', () => {
        expect(new ShallowRenderer().render(<SearchResult matches={[]} />)).toMatchSnapshot()
    });
});
