import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import MatchViewer from "./MatchViewer.js";
import PropTypes from "prop-types";
import Badge from 'react-bootstrap/Badge'

const SearchResult = props => {
    const { matches } = props;

    return (
        <div className="container">
                <div className={"col-sm-1 col-xl-2 align-items-end"}>
                    <h6>
                        Matches found <Badge variant="secondary">{matches.length}</Badge>
                    </h6>
                </div>
            <div className="row align-items-center">
                { matches.map(match => (
                    <div key={match.id} className="col-sm-6 col-md-3 col-lg-4" >
                        <MatchViewer match={match} />
                    </div>
                ))};
            </div>
        </div>
    )
};

SearchResult.propTypes = {
    matches: PropTypes.array
};

SearchResult.defaultProps = {
    matches: []
};

export default SearchResult
