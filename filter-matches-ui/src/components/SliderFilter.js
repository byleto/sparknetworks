import React from 'react'
import PropTypes from 'prop-types'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'rc-slider/assets/index.css';
import Slider from 'rc-slider'
import Card from 'react-bootstrap/Card';
const createSliderWithTooltip = Slider.createSliderWithTooltip;
const SliderWithToolTip = createSliderWithTooltip(Slider);

class SliderFilter extends React.Component {
    constructor(props) {
        super(props);
        const { max } = props
        this.state = {
            value: max,
        };
    }

    onSliderChange = (value) => {
      this.setState({
        value,
      });
      this.props.onChange(this.props.name, value);
    }

    render() {
        const { min, max, label } = this.props
        return (
            <Card style={{ width: '17rem' }}>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <strong>{label}</strong>
                        </div>
                        <div className="col-sm-10 offset-sm-1">
                            <SliderWithToolTip 
                                allowCross={false} 
                                min={min} max={max} 
                                value={this.state.value} 
                                onChange={this.onSliderChange} 
                            />
                        </div>
                        <div className="col-sm-12">
                            <label>
                                {`${this.state.value} Km`}
                            </label> 
                        </div>
                    </div>
                </div>
            </Card>
        );
    }
  }

SliderFilter.propTypes = {
    label: PropTypes.string,
    min: PropTypes.number,
    max: PropTypes.number,
    name: PropTypes.string,
    onChange: PropTypes.func
}

SliderFilter.defaultProps = {
    label: '',
    min: 0,
    max: 0,
    name: '',
    onChange: () => {}
}

export default SliderFilter